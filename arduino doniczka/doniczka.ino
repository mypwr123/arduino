int digitalSensor = A0;
int digitalSensor2 = A1;

int pump = 2;              //przekaznik
int pump2 = 3;

float sensor1Value = 0;    // wartosc z czujnika
float sensor1Value2 = 0;

void setup() {
  pinMode(digitalSensor, INPUT);
  pinMode(digitalSensor2, INPUT);
  pinMode(pump, OUTPUT);
  pinMode(pump2, OUTPUT);

  Serial.begin(9600);
}

void loop() {
  sensor1Value = analogRead(A0);
  
  Serial.println(analogRead(A0));
  
  if (sensor1Value > 400) {   //im wieksza wartosc tym bardziej sucho
    digitalWrite(pump, LOW);  //wlacz
    delay(12000);            
    digitalWrite(pump, HIGH); //wylacz
  } else {
    digitalWrite(pump, HIGH);
  }
  delay(1000);
  
  Serial.println(analogRead(A1));
  
  sensor1Value2 = analogRead(A1);

  if (sensor1Value2 > 400) {   //im wieksza wartosc tym bardziej sucho
    digitalWrite(pump2, LOW);
    delay(10000);
    digitalWrite(pump2, HIGH);
  } else {
    digitalWrite(pump2, HIGH);
  }
   
  delay(3600000);              //godzian w milisekundach

}
