#include <CheapStepper.h>

#define butt 2
#define czuj A2

int  stan = 0, clicklenght = 0;
bool blokada = false;

CheapStepper stepper(8, 9, 10, 11);                     //piny motora

void setup() {

  pinMode(czuj, INPUT);                                 //pin czujnika
  pinMode(butt, INPUT);

  //Serial.begin(9600);

  stepper.setRpm(12);                                  //predkosc silnika
}
void loop() {

//  Serial.println(analogRead(czuj));
//  Serial.println(digitalRead(butt));

  if (stan == 1 && analogRead(czuj) < 300 && blokada == 0) {        //sprawdza czy jest zwinieta, czy swieci, czy niema blokady
    stepper.moveDegreesCCW(180);                                    //180stopni przeciwko zegarowi
    delay(1000);
    stan = 0;
  }

  if (digitalRead(butt) == 1) {

    clicklenght = 0;
    while (digitalRead(butt) == 1) {                    //dopuki jest kilkniete to licz jak dlugo jest klikniete
      delay(100);
      clicklenght = clicklenght + 100;
    }

  //  Serial.println(clicklenght);
  //  Serial.println("mili");
  
    if (clicklenght > 2000) {                           //jeżeli powyżej 2sekund blokawa włączona
      blokada = 1;
    }
    else {
      blokada = 0;

    }
    if (stan == 0) {
      stepper.moveDegreesCW(200);                         //200stopni z zegarem
      delay(1000);
      stan = 1;
    }
    else if (stan == 1) {
      stepper.moveDegreesCCW(200);                        //200stopni przeciwko zegarowi
      delay(1000);
      stan = 0;

    }
    else {}

  }
  delay(10);
}
